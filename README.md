# NetLogo Simulation Projects

NetLogo is a tool used to program one's own simulations using their built in programming language (LOGO). 
The raw text file representation of these files will appear unreadable to most users. Please visit the
NetLogo website listed below to download the IDE. 

> [NetLogo Download](https://ccl.northwestern.edu/netlogo/)

## Projects

- [Encore Simulation](Encore) - Project to simulate the behavior of the phenomenon known as an encore.
- [Farming Simulation](FarmingSimulation) - Project to simulate plant success/failure based on several controllable factors on a farm.
- [Game Theory](GameTheory) - Utilizing several game theory principles to simulate outcomes for Europe during the refugee crisis.
- [Parking Lot Behavior](ParkingLotBehavior) - Simulation showing how people tend to find a parking spot in a parking lot.
- [PredatorPrey](PredatorPrey) - Simple simulation of predator/prey.

## Executing Simulations

After the user has downloaded and installed [NetLogo](https://ccl.northwestern.edu/netlogo/), the user can run any of the nlogo files
by loading the file to the IDE and clicking the run or simulation button. The user can also adjust some of the parameters in the 
simulations using the built in buttons and sliders.